'use strict';

module.exports = {
	app: {
		title: 'UC Davis FM:ECO',
		description: 'Facilities Management: Energy Conservation Office',
		keywords: 'facilities, management, seseme, ceed, water, energy, campus, dashboard'
	},
	templateEngine: 'swig',
	assets: {
		lib: {
			css: [
				'appEnergy/public/lib/bootstrap/dist/css/bootstrap.min.css',
				'appEnergy/public/lib/bootstrap/dist/css/bootstrap-theme.min.css'
			],
			js: [
				'appEnergy/public/lib/angular/angular.min.js',
				'appEnergy/public/lib/angular-resource/angular-resource.min.js',
				'appEnergy/public/lib/angular-animate/angular-animate.min.js',
				'appEnergy/public/lib/angular-ui-router/release/angular-ui-router.min.js',
                'appEnergy/public/lib/angular/angular-route.min.js',
				'appEnergy/public/lib/angular-ui-utils/ui-utils.min.js',
				'appEnergy/public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
				'appEnergy/public/lib/d3/d3.min.js',
				'appEnergy/public/lib/jquery/dist/jquery.min.js',
				'appEnergy/public/lib/highstock-release/highstock.js',
				'appEnergy/public/lib/spin.min.js',
                'appEnergy/public/lib/highstock-release/highcharts-more.js'
			]
		},
		css: [
            'appEnergy/public/dist/application.min.css'
		],
		js: [
			'appEnergy/public/dist/application.min.js'
		]
	}
};