'use strict';
var init = require('./init')(),
    config = require('./config'),
    express = require('./express'),
    http = require('http');
var WebApplication = function(appName,domainNames) {
    this.appName = appName;
    this.domainNames = domainNames;
    this.appMain = express.createApp(appName);
};
WebApplication.prototype.setPort = function(portNum) {
    (this.appMain).set('port',portNum).listen((this.appMain).get('port'));
    console.log('\x1b[35m',(this.appName)+' is running on port '+(this.appMain).get('port'));
};
module.exports = WebApplication;