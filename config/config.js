'use strict';
var _ = require('lodash'),
    glob = require('glob');
module.exports = function(routePath){
    var module = require('./env/'+routePath+'/'+process.env.NODE_ENV);
    module.getGlobbedFiles = function(globPatterns,removeRoot,routePath){
        var _this = this,
            urlRegex = new RegExp('^(?:[a-z]+:)?//','i'),
            output = [];
        // if an array, act recursively to glob assets and server files
        if (_.isArray(globPatterns)){
            globPatterns.forEach(function(globPattern){
                if (globPattern.search(routePath)>-1) {
                    output = _.union(output,_this.getGlobbedFiles('./'+globPattern,'./'+routePath+'/public/',routePath));
                } else if (globPattern.search('lib')>-1) {
                    output = _.union(output,_this.getGlobbedFiles('./'+globPattern,'./public/',routePath));
                }
            });
        } else if (_.isString(globPatterns)) {
            if (urlRegex.test(globPatterns)) {
                output.push(globPatterns);
            } else {
                glob(globPatterns,{
                    sync: true
                },function(err,files) {
                    if (removeRoot) {
                        files = files.map(function(file) {
                            return file.replace(removeRoot,'');
                        });
                    }
                    output = _.union(output,files);
                });
            }
        }
        return output;
    };
    module.getJavaScriptAssets = function(includeTests,routePath) {
        var output = this.getGlobbedFiles(this.assets.lib.js,'public/',routePath);
        output = output.concat(this.getGlobbedFiles(this.assets.js,routePath+'/public/',routePath));
        return output;
    };
    module.getCSSAssets = function(routePath) {
        var output = this.getGlobbedFiles(this.assets.lib.css,'public/',routePath);
        output = output.concat(this.getGlobbedFiles(this.assets.css,routePath+'/public/',routePath));
        return output;
    };
    return module;
};