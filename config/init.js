'use strict';
var glob = require('glob');
module.exports = function() {
    // set the environment variable (NODE_ENV)
    // Windows: set NODE_ENV={development|test|production}
    // Mac: export NODE_ENV={development|test|production}
    glob('./config/env/*/'+process.env.NODE_ENV+'.js',{sync:true},function(err,environmentFiles) {
        if (!environmentFiles.length) {
            if (process.env.NODE_ENV) {
                console.log('\x1b[31m','No config file found for "'+process.env.NODE_ENV+'" environment, default to development instead.');
            } else {
                console.log('\x1b[31m','NODE_ENV not defined! Default to development instead.');
            }
            process.env.NODE_ENV = 'development';
        } else {
            console.log('\x1b[34m','Using "'+process.env.NODE_ENV+'" environment configuration');
        }
    });
    // add node server extensions
    require.extensions['ServerController.js'] = require.extensions['.js'];
    require.extensions['ServerModel.js'] = require.extensions['.js'];
    require.extensions['ServerRoutes.js'] = require.extensions['.js'];
};