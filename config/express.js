'use strict';
var express = require('express'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    compress = require('compression'),              //gzip compression
    methodOverride = require('method-override'),
    cookieParser = require('cookie-parser'),
    helmet = require('helmet'),
    flash = require('connect-flash'),
    consolidate = require('consolidate'),
    path = require('path'),
    //passport = require('passport'), //use for authorization
    //session = require('express-session'), //use for sessions
    vhost = require('vhost');
module.exports.createApp = function(routePath) {
    // init express application
    var app = express(),
        config = require('./config')(routePath);
    // glob model files
    config.getGlobbedFiles('./'+routePath+'/models/**/*.js').forEach(function(modelPath) {
        require(path.resolve(modelPath));
    });
    // pass app info and assets to environment locals
    app.locals.title = config.app.title;
    app.locals.description = config.app.description;
    app.locals.keywords = config.app.keywords;
    app.locals.jsFiles = config.getJavaScriptAssets(false,routePath);
    app.locals.cssFiles = config.getCSSAssets(routePath);
    // pass res url to environment locals
    app.use(function(req,res,next) {
        res.locals.url = req.protocol+'://'+req.headers.host+req.url;
        next();
    });
    // gzip compression - must place before express.static
    app.use(compress());
    //app.use(compress({
    //    filter: function(req,res){return (/json|text|javascript|css|svg|html/).test(res.getHeader('Content-Type'));},
    //    level: 9
    //}));
    // display stack errors
    app.set('showStackError',true);
    // set swig as template engine
    app.engine('html',consolidate[config.templateEngine]);
    // set views path and view engine
    app.set('view engine','html');
    app.set('views','./'+routePath+'/views');
    // environment middleware
    if (process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));         // morgan logger in terminal
        app.set('view cache',false);    // disable views cache
    } else if (process.env.NODE_ENV === 'production') {
        app.locals.cache = 'memory';
    } else if (process.env.NODE_ENV === 'test') {
        app.locals.cache = 'memory';
    }
    // must have request body parser middleware before methodOverride
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    app.use(methodOverride());
    // enable jsonp
    app.enable('jsonp callback');
    // must have cookie parser above session
    app.use(cookieParser());
    // use flash for flash messages
    app.use(flash());
    // secure express headers with helmet
    app.use(helmet.xframe());
    app.use(helmet.iexss());
    app.use(helmet.contentTypeOptions());
    app.use(helmet.ienoopen());
    app.disable('x-powered-by');
    // set static folders and application routes
    app.use(express.static(path.resolve('./'+routePath+'/public')));
    // glob routing files
    config.getGlobbedFiles('./'+routePath+'/routes/**/*.js').forEach(function(routePath) {
        require(path.resolve(routePath))(app);
    });
    // set 500 SERVER ERROR page
    app.use(function(err,req,res,next) {
        if (!err) return next();
        console.error(err.stack);
        res.status(500).render('500ServerView',{error:err.stack});
    });
    // set 404 NOT FOUND page
    app.use(function(req,res) {
        res.status(404).render('404ServerView',{url:req.originalUrl,error:'Not Found'});
    });
    return app;
};
// in the case that there are multiple express applications, use virtual host
module.exports.createVHostApp = function(appsArray,env) {
    var app = express();
    appsArray.forEach(function(appObj){
        (appObj.domainNames).forEach(function(domainName){
            console.log('\x1b[36m',domainName);
            app.use(vhost(domainName,appObj.appMain));
        });
    });
    return app;
};