'use strict';
var config = require('./config'),
    express = require('./express');
var VHostApplication = function(vHostName, webAppsArray){
    this.vHostName = vHostName;
    this.vHostMain = express.createVHostApp(webAppsArray);
};
VHostApplication.prototype.setPort = function(portNum) {
    (this.vHostMain).set('port',portNum).listen((this.vHostMain).get('port'));
    console.log('\x1b[33m',(this.vHostName)+' is running on port '+(this.vHostMain).get('port'));
};
module.exports = VHostApplication;