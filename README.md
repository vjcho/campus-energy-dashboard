![Build Status](https://img.shields.io/badge/build-passed-green.svg?style=flat-square)
![devDependencies](https://img.shields.io/badge/devDependencies-derp-orange.svg?style=flat-square)

FMECO-WATER contains a JavaScript web applications built on [Node.js](http://www.nodejs.org/), [Express](http://expressjs.com/), and [AngularJS](http://angularjs.org/). Campus Water Dashboard.

## Table of Contents

- [Installation](#Installation)
- [Configuration](#Configuration)
- [Documentation](#Documentation)
- [Credits](#Credits)

## Installation

- [Download the latest release](https://bitbucket.org/ucdfacilitiesmanagementit/fmeco-water).
- Clone the repo: `git clone git@bitbucket.org:ucdfacilitiesmanagementit/fmeco-water.git`.
- Install [node.js](http://nodejs.org).
- Install [npm](https://www.npmjs.org).
- Install [Bower](http://bower.io): `npm install -g bower`.
- Install [Grunt](http://gruntjs.com): `npm install -g grunt-cli`.
- Install `package.json` dependencies: `npm install`.
- Install `bower.json` dependencies: `bower install`.
- Copy over the necessary /public/lib files: `grunt librarycopy`.

### What's Included

After installation is complete, you should see directories/files organized like the following:

```
fmeco-water/
├── app/
│   ├── controllers/
│   ├── models/
│   ├── public/
│   │   ├── dist/
│   │   │   └── application.min.js
│   │   ├── lib/
│   │   │   └── (based on /public/lib & /config/(NODE_ENV) & bower.json)
│   │   ├── modules/
│   │   │   ├── config/
│   │   │   ├── controllers/
│   │   │   ├── css/
│   │   │   ├── img/
│   │   │   ├── sass/
│   │   │   ├── services/
│   │   │   ├── views/
│   │   │   └── homeClientModule.js
│   │   ├── application.js
│   │   ├── config.js
│   │   ├── humans.txt
│   │   └── robots.txt
│   ├── routes/
│   └── views/
├── public/lib
│   └── (based on bower.json)
├── config/
│   ├── env/
│   │   ├── all.js
│   │   ├── development.js
│   │   ├── test.js
│   │   └── production.js
│   ├── config.js
│   ├── express.js
└── node_modules/
    └── (based on package.json)
```

When using `grunt build` in the `NODE_ENV=development`, you build a minified version of the front-end AngularJS application.

## Configuration

Currently there are 6 differently repositories set to run on the same server.

* The CEED application should run on port 1334. Go to [http://localhost:1334](http://localhost:1334)
* The Energy and Water Challenge application should run on port 1335. Go to [http://localhost:1335](http://localhost:1335)
* The Water Dashboard application should run on port 1336. Go to [http://localhost:1336](http://localhost:1336)
* The Thermal Feedback application should run on port 1337. Go to [http://localhost:1337](http://localhost:1337)
* The ECO Agent application should run on port 1338. Go to [http://localhost:1339](http://localhost:1338)
* Virtual House should run on port 1333

### Development
Best environment to work on the front-end and not require the database to be on locally.

- Install and configure everything on local development server as [installation](#installation).
- On Mac or Linux, run `export NODE_ENV=development`. On Windows, run `set NODE_ENV=development`.
- Based on the necessary task, run by default `grunt` or `grunt build` or whatever else as needed.
- NOTE: By default, Grunt will run the jshint and csslint. Check `gruntfile.js` to see other tasks.

### Production
The final stage where everything is minified and the database privileges are set.

- Install and configure everything on the production server as [installation](#installation).
- Install [Forever](https://github.com/foreverjs/forever): `npm install -g forever`.
- When ready, run `forever start server.js`. When ready to stop, run `forever stopall` or just `forever stop server.js`.

## Documentation

Check out the [Campus Energy Feedback System](https://ucdavis.jira.com/wiki/display/FMM/Campus+Energy+Feedback+System) page on Confluence. :)

## Credits

### Developers

*Liz Shigetoshi*

*Matt Sidor*

*Valerie Cho*

### Designer
*Jessica Blizard*

### Data Analyst

*Dan Colvin*

### Project Managers

*Kiernan Salmon*

*Joshua MoreJohn*

*David Trombly*

### Affiliations

*Energy Conservation Office*

*Utilities*

*Student Housing*

### Special Thanks
*Emily Hsieh, Elisha St. Denis*