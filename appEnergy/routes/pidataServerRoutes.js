'use strict';
var piSoapData = require('../../appEnergy/controllers/soapPiDataServerController.js'),
	piRestData = require('../../appEnergy/controllers/restPiDataServerController.js');
	//testing = require('../../appEnergy/controllers/piDataController.js');
module.exports = function(app) {
	app.get('/piSoapData',piSoapData.getDemandValues);
	app.get('/piRestData',piRestData.getDemandValues);
	//app.get('/testing', testing.getDemandValues);
};