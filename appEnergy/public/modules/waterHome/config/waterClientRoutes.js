'use strict';

// Setting up route
angular.module('water').config(['$stateProvider','$urlRouterProvider',
	function($stateProvider,$urlRouterProvider) {

		$urlRouterProvider.otherwise('/');

		// Allow case insensitive urls
	   	$urlRouterProvider.rule(function ($injector, $location) {
	        var path = $location.path(), normalized = path.toLowerCase();
	        if (path !== normalized) {
	            $location.replace().path(normalized);
	        }
	    });

		// Water home state routing
		$stateProvider.
        state('home', {
                url: '/',
                templateUrl: 'modules/waterHome/views/waterGoalsClientView.html'
            }).state('graphs', {
                url: '/graphs',
                templateUrl: 'modules/waterHome/views/waterGraphsClientView.html'
            });
	}
]);