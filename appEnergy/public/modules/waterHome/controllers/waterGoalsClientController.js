'use strict';
angular.module('waterHome').controller('GoalsController', ['$scope', '$location', '$state', '$http', '$q',
    function ($scope,$location,$state,$http,$q) {
        var monthvisible, annualvisible;
        var buttonIDs = ['#RTButton','#monthlyButton','#annualUseButton','#annualCostButton','#annualGHGButton'];
        var graphIDs = ['#RTAreaGraph','#monthlyAreaGraph','#annualUseGraph','#annualCostGraph','#annualGHGGraph'];

        var renewkwh= [5028386,3956201,1166153,1391627,907445,159667,299485,365732,503593,1592668,2915799,3767009];
        var ELkwh = [16845934,17045839,18671287,18552013,16286515,17236253,17143595,15825268,17780807,16372412,15703761,16823551];
        var onsitekwh = [128363,116181,98139,88384,58480,36970,57636,67270,103195,125452,195915,227676];

        var annualRenewKwh = [22397063,34572243,39621673,43647683,49587283,49883195,45239617,33568453,27280246,31919097,37135977,29460043,71191697,41399030,30581180,29109965,27443253,41775586,44354029,40234471,24552755,22053765];
        var annualELKwh = [139370944,129788013,132274055,132848371,126269247,131265920,143061127,152157653,160742479,168495474,177402952,191429456,161785663,198520450,212911300,209217955,206617587,189900974,190177691,189131249,202449565,204287235];
        var annualOnsiteKwh = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,677414,1209791,1324720,1303662];

        var testArr = [];

        var CHCPMonthly,masterMonthly,onsiteMonthly,renewMonthly,ELMonthly;
        var gasAnnual,onsiteAnnual,renewAnnual,ELAnnual;

        $scope.navbarCollapsed = false;

        function loadSpinner(htmlID) {
            var opts = {
                lines: 10, // The number of lines to draw
                length: 7, // The length of each line
                width: 4, // The line thickness
                radius: 10, // The radius of the inner circle
                rotate: 0, // The rotation offset
                color: '#000', // #rgb or #rrggbb
                speed: 1, // Rounds per second
                trail: 60, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9 // The z-index (defaults to 2000000000)
            };
            var spinner = new Spinner(opts).spin();
            var element = document.getElementById(htmlID);
            //console.log(element);
            element.innerHTML = '';
            element.appendChild(spinner.el);
        }

        function toSec(num){ //function to get date to nearest tenth of second
            var result = num - num%10000;
            return result;
        }

        function makeSeries(names, data, colors){
            var series = [];
            for(var i = 0; i < data.length; i++){
                series.push({
                    name: names[i],
                    data: data[i],
                    color: colors[i]
                })
            }
            return series;
        }

        function resetMainButtons(num){
            for(var i = 0; i < buttonIDs.length; i++){
                if(i == num)
                    $(buttonIDs[i]).addClass('active');
                else
                    $(buttonIDs[i]).removeClass('active');
            }
        }

        function switchtoKWH(chart){
            if(monthvisible){
                chart.series[2].update({
                    data: onsitekwh,
                    tooltip:{
                        valueSuffix: ' kWh'
                    }
                });
                chart.series[3].update({
                    data: renewkwh,
                    tooltip:{
                        valueSuffix: ' kWh'
                    }
                });
                chart.series[4].update({
                    data: ELkwh,
                    tooltip:{
                        valueSuffix: ' kWh'
                    }
                });
            }
            else if(annualvisible){
                chart.series[1].update({
                    data: annualOnsiteKwh,
                    tooltip:{
                        valueSuffix: ' kWh'
                    }
                });
                chart.series[2].update({
                    data: annualRenewKwh,
                    tooltip:{
                        valueSuffix: ' kWh'
                    }
                });
                chart.series[3].update({
                    data: annualELKwh,
                    tooltip:{
                        valueSuffix: ' kWh'
                    }
                });
            }
            chart.yAxis[0].update({
                title: {
                    text: 'kWh'
                }
            });
        }

        function switchtoMMBTU(chart){
            if(monthvisible){
                chart.series[2].update({
                    data: onsiteMonthly,
                    tooltip:{
                        valueSuffix: ' MMBtu'
                    }
                });
                chart.series[3].update({
                    data: renewMonthly,
                    tooltip:{
                        valueSuffix: ' MMBtu'
                    }
                });
                chart.series[4].update({
                    data: ELMonthly,
                    tooltip:{
                        valueSuffix: ' MMBtu'
                    }
                });
            }
            else if(annualvisible){
                chart.series[1].update({
                    data: onsiteAnnual,
                    tooltip:{
                        valueSuffix: ' MMBtu'
                    }
                });
                chart.series[2].update({
                    data: renewAnnual,
                    tooltip:{
                        valueSuffix: ' MMBtu'
                    }
                });
                chart.series[3].update({
                    data: ELAnnual,
                    tooltip:{
                        valueSuffix: ' MMBtu'
                    }
                });
            }
            chart.yAxis[0].update({
                title: {
                    text: 'MMBtu'
                }
            });
        }

        $.ajax({
            type: "GET",
            url: "modules/waterHome/controllers/asdf.csv",
            dataType: "text",
            success: function(data){
                loadStaticData(data);
            }
        });


        function loadStaticData(data){
            var split = data.split(/\r\n|\n/);
            var one = split[0].split(',');
            //var two = split[1].split(',');
            console.log(split[1]);
            //console.log(two);
        }



        $('#usageDesc').hide();
        $('#costDesc').hide();
        //$('#GHGDesc').hide();
        $('#EGtoggle').hide();


        $('#monthlyButton').click(function(){
            monthvisible = true;
            annualvisible = false;
            $('#annualUseGraph').hide(600);
            $('#annualCostGraph').hide(600);
            $('#annualGHGGraph').hide(600);
            $('#monthlyAreaGraph').show();
            $('#RTGraph').hide(600);
            $('#usageDesc').hide();
            $('#costDesc').hide();
            $('#GHGDesc').hide();
            resetMainButtons(1);
            $('#EGtoggle').show();
            setInterval(function(){
                $('#monthlyAreaGraph').highcharts().reflow();
            },10);

            //$('#monthlyButton').addClass('active');
            //$('#RTButton').removeClass('active');
            //$('#annualUseButton').removeClass('active');
            //$('#annualCostButton').removeClass('active');
            //$('#annualGHGButton').removeClass('active');
        });

        $('#annualUseButton').click(function(){
            annualvisible = true;
            monthvisible = false;
            $('#annualUseGraph').show();
            $('#annualCostGraph').hide(600);
            $('#annualGHGGraph').hide(600);
            $('#monthlyAreaGraph').hide(600);
            $('#RTGraph').hide(600);
            $('#usageDesc').show();
            $('#costDesc').hide();
            //$('#GHGDesc').hide();
            resetMainButtons(2)
            $('#EGtoggle').show();
            setInterval(function(){
                $('#annualUseGraph').highcharts().reflow();
            },10);
        });

        $('#annualCostButton').click(function(){
            monthvisible = false;
            annualvisible = false;
            $('#annualUseGraph').hide(600);
            $('#annualCostGraph').show();
            $('#annualGHGGraph').hide(600);
            $('#monthlyAreaGraph').hide(600);
            $('#RTGraph').hide(600);
            $('#usageDesc').hide();
            $('#costDesc').show();
            //$('#GHGDesc').hide();
            resetMainButtons(3)
            $('#EGtoggle').hide();
            setInterval(function(){
                $('#annualCostGraph').highcharts().reflow();
            },1);
        });

        //$('#annualGHGButton').click(function(){
        //    monthvisible = false;
        //    annualvisible = false;
        //    $('#annualUseGraph').hide(600);
        //    $('#annualCostGraph').hide(600);
        //    $('#annualGHGGraph').show(600);
        //    $('#monthlyAreaGraph').hide(600);
        //    $('#RTGraph').hide(600);
        //    $('#usageDesc').hide();
        //    $('#costDesc').hide();
        //    //$('#GHGDesc').show();
        //    resetMainButtons(4);
        //    $('#EGtoggle').hide();
        //});

        $('#RTButton').click(function(){
            monthvisible = false;
            annualvisible = false;
            $('#annualUseGraph').hide(600);
            $('#annualCostGraph').hide(600);
            $('#annualGHGGraph').hide(600);
            $('#monthlyAreaGraph').hide(600);
            $('#RTGraph').show(600);
            $('#usageDesc').hide();
            $('#costDesc').hide();
            //$('#GHGDesc').hide();
            resetMainButtons(0);
            $('#EGtoggle').hide();
        });

        //electricity and gas toggle buttons
        $('#gasButton').click(function(){
            if(monthvisible){
                var monthgraph = $('#monthlyAreaGraph').highcharts();
                monthgraph.series[0].show();
                monthgraph.series[1].show();
                monthgraph.series[2].hide();
                monthgraph.series[3].hide();
                monthgraph.series[4].hide();
                $('#gasButton').addClass('active');
                $('#elButton').removeClass('active');
                $('#bothButton').removeClass('active');
            }
            else if(annualvisible){
                var annualgraph = $('#annualUseGraph').highcharts();
                annualgraph.series[0].show();
                annualgraph.series[1].hide();
                annualgraph.series[2].hide();
                annualgraph.series[3].hide();
                $('#gasButton').addClass('active');
                $('#elButton').removeClass('active');
                $('#bothButton').removeClass('active');
            }
        });

        $('#elButton').click(function(){
            if(monthvisible){
                var monthgraph = $('#monthlyAreaGraph').highcharts();
                monthgraph.series[0].hide();
                monthgraph.series[1].hide();
                //console.log(monthgraph.series[3].data);
                switchtoKWH(monthgraph);
                monthgraph.series[2].show();
                monthgraph.series[3].show();
                monthgraph.series[4].show();
                $('#gasButton').removeClass('active');
                $('#elButton').addClass('active');
                $('#bothButton').removeClass('active');
            }
            else if(annualvisible){
                var annualgraph = $('#annualUseGraph').highcharts();
                annualgraph.series[0].hide();
                switchtoKWH(annualgraph);
                annualgraph.series[1].show();
                annualgraph.series[2].show();
                annualgraph.series[3].show();
                $('#gasButton').removeClass('active');
                $('#elButton').addClass('active');
                $('#bothButton').removeClass('active');
            }
        });

        $('#bothButton').click(function(){
            if(monthvisible){
                var monthgraph = $('#monthlyAreaGraph').highcharts();
                switchtoMMBTU(monthgraph);
                monthgraph.series[0].show();
                monthgraph.series[1].show();
                monthgraph.series[2].show();
                monthgraph.series[3].show();
                monthgraph.series[4].show();
                //console.log(monthgraph.series[3].data[0]);
                $('#gasButton').removeClass('active');
                $('#elButton').removeClass('active');
                $('#bothButton').addClass('active');
            }
            else if(annualvisible){
                var annualgraph = $('#annualUseGraph').highcharts();
                switchtoMMBTU(annualgraph);
                annualgraph.series[0].show();
                annualgraph.series[1].show();
                annualgraph.series[2].show();
                annualgraph.series[3].show();
                $('#gasButton').removeClass('active');
                $('#elButton').removeClass('active');
                $('#bothButton').addClass('active');
            }
        });



        loadSpinner('RTAreaGraph');
        createRTGraph();
        createStaticGraphs();


        function createRTGraph() {

            var BioDemand = $http.get('https://ucd-pi-iis.ou.ad3.ucdavis.edu/piwebapi/streams/P09KoOKByvc0-uxyvoTV1UfQl14AAAVVRJTC1QSS1QXEJJT0RJR0VTVEVSX0RFTUFORF9NTUJUVS9I/interpolated?startTime=*-1mo&endTime=*&interval=15m');
            var PGEDemand = $http.get('https://ucd-pi-iis.ou.ad3.ucdavis.edu/piwebapi/streams/P09KoOKByvc0-uxyvoTV1UfQoV4AAAVVRJTC1QSS1QXFBHRV9TVUJTVEFUSU9OX0RFTUFORF9NTUJUVS9I/interpolated?startTime=*-1mo&endTime=*&interval=15m');
            var SolarDemand = $http.get('https://ucd-pi-iis.ou.ad3.ucdavis.edu/piwebapi/streams/P09KoOKByvc0-uxyvoTV1UfQnF4AAAVVRJTC1QSS1QXFNPTEFSX0ZBUk1fREVNQU5EX01NQlRVL0g/interpolated?startTime=*-1mo&endTime=*&interval=15m');
            var MasterDemand = $http.get('https://ucd-pi-iis.ou.ad3.ucdavis.edu/piwebapi/streams/P09KoOKByvc0-uxyvoTV1UfQ3F4AAAVVRJTC1QSS1QXFBHRS5NQVNURVJNRVRFUlNfR0FTX1RPVEFMX0RFTUFORF9NTUJUVS9I/interpolated?startTime=*-1mo&endTime=*&interval=15m');
            var CHCPDemand = $http.get('https://ucd-pi-iis.ou.ad3.ucdavis.edu/piwebapi/streams/P09KoOKByvc0-uxyvoTV1UfQ1V4AAAVVRJTC1QSS1QXENIQ1AuR0FTX0RFTUFORF9NTUJUVS9I/interpolated?startTime=*-1mo&endTime=*&interval=15m');

            $q.all([PGEDemand, SolarDemand, BioDemand, CHCPDemand, MasterDemand]).then(function (result) {
                var masterAr = [];
                var tmp = [];
                angular.forEach(result, function (response) {
                    for (var i = 0; i < response.data["Items"].length; i++) {
                        var value;
                        if (response.data["Items"][i]["Value"]["Name"] == 'No Data' || response.data["Items"][i]["Value"]["Name"] == 'Pt Created' || response.data["Items"][i]["Value"]["Name"] == 'Calc Failed')
                            value = 0;
                        else
                            value = response.data["Items"][i]["Value"];
                        tmp.push([toSec(Date.parse(response.data["Items"][i]["Timestamp"])), value]);

                    }
                    masterAr.push(tmp);
                    tmp = [];
                });
                makeHighchart('#RTAreaGraph', masterAr);
                //var chart = $('#RTAreaGraph').highcharts();
                //chart.xAxis[0].min = new Date().getTime() - (24 * 3600 * 1000);
                //chart.redraw();
            });
        }


        function createStaticGraphs(){
            //make monthly graph
            renewMonthly = [17157, 13499, 3979, 4748, 3096, 545, 1022, 1248,1718,5434,9949,12853]; //green
            ELMonthly = [57478,58160,63706,63299,55570,58810,58494,53996,60668,55863,53581,57402]; //brown
            onsiteMonthly = [438,396,335,302,200,126,197,230,352,428,668,777]; //solar + biodigester
            masterMonthly = [11103,12065,12327,17198,13431,20738,24066,20203,15576,18035,17118,13997];
            CHCPMonthly = [44029,44647,44707,54271,79060,95469,107092,74701,67122,65749,60597,43940];


            //for(var i = 0; i < renewMonthly.length; i++){
            //    renewkwh.push(renewMonthly[i]*1000000/3412);
            //    ELkwh.push(ELMonthly[i]*1000000/3412);
            //    onsitekwh.push(onsiteMonthly[i]*1000000/3412);
            //}
            //console.log(renewkwh);
            var staticMonthly = [CHCPMonthly,masterMonthly,onsiteMonthly,renewMonthly,ELMonthly];

            //console.log(sharedTheme.theme);

            var names = ['CHCP Gas','Master Meter Gas', 'Onsite Renewable Electricity', 'Renewable Electricity', 'Electricity',];
            var colors = ['#0CB7F7', '#D301FE','#E8E762','#A2DF0E','#FDAF02'];
            makeStaticChart('#monthlyAreaGraph', makeSeries(names, staticMonthly, colors), '', ' MMBtu');
            var chart = $('#monthlyAreaGraph').highcharts();
            //chart.series[5].hide();
            //chart.series[6].hide();
            //chart.series[7].hide();
            chart.xAxis[0].setCategories(['Jul','Aug','Sep','Oct','Nov','Dec','Jan','Feb','Mar','Apr','May','Jun']);
            chart.setTitle({text: "Monthly Usage"});
            chart.xAxis[0].update({
                title: {
                    text: '2014-15'
                }
            });


            renewAnnual = [76419,117961,135189,148926,169192,170202,154358,114536,93080,108908,126708,100518,242906,141254,104343,99323,93636,142538,151336,137280,83774,75247];
            ELAnnual = [475534,442837,451319,453279,430831,447879,488125,519162,548453,574907,605299,653157,552013,677352,726453,713852,704979,647942,651198,649444,695278,701476];
            onsiteAnnual = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2311,4128,4520,4448];
            gasAnnual = [1295861,1339239,1367739,1518401,1492648,1558929,1585038,1720063,1659405,1470126,1596160,1558921,1424165,1349479,1213520,1131053,1146230,1108043,1097641,1057601,1031904,977242];
            var staticAnnual = [gasAnnual,onsiteAnnual,renewAnnual,ELAnnual];

            names = [ 'Gas', 'Onsite Renewable Electricity','Renewable Electricity', 'Electricity'];
            colors = ['#D301FE','#E8E762','#A2DF0E','#FDAF02'];
            makeStaticChart('#annualUseGraph', makeSeries(names, staticAnnual, colors), '', ' MMBtu');
            var annChart = $('#annualUseGraph').highcharts();
            annChart.setTitle({text: 'Annual Energy Usage'});
            annChart.xAxis[0].update({
                title: {
                    text: "Fiscal Year"
                }
            });


            var ELCost = [6742678,5961753,5648060,4303813,3780223,4932591,4958330,6061833,10724107,11173420,8240852,11475201,14045113,15725249,17471935,17951193,16638421,15299099,15522110,15174723,16497191,17351689];
            var gasCost = [3931441,3294544,3309407,4081089,4384729,4597367,5137465,15448939,9207182,8057666,9446424,10530559,12256573,10906763,10782996,9613639,8662684,7354977,7430879,8940965,7523800,6046685];
            var staticCost = [gasCost,ELCost];
            names = ['Gas','Electricity'];
            colors = ['#D301FE','#E8E762'];
            makeStaticChart('#annualCostGraph', makeSeries(names, staticCost, colors), '$', '');
            var costChart = $('#annualCostGraph').highcharts();
            costChart.setTitle({text: 'Annual Cost'});
            costChart.yAxis[0].update({
                title:{
                    text: "Cost"
                }
            });
            costChart.xAxis[0].update({
                title:{
                    text: "Fiscal Year"
                }
            });


            //var ELGHG = [38774,36108,36799,36959,35129,36519,39800,42331,44720,46876,49355,53257,45010,55230,59233,58206,57482,52832,53097,52954,56691,57197];
            //var gasGHG = [68829,71133,72647,80650,79282,82802,84189,91361,88139,78085,84780,82802,75644,71677,64456,60076,60882,58853,58301,56174,54809,51906];
            //var staticGHG = [gasGHG,ELGHG];
            //makeStaticChart('#annualGHGGraph', makeSeries(names, staticGHG, colors), '', ' MT CO2e');
            //var GHGChart = $('#annualGHGGraph').highcharts();
            //
            //GHGChart.yAxis[0].update({
            //    title:{
            //        text: "GHG"
            //    }
            //});
            //GHGChart.xAxis[0].update({
            //    title:{
            //        text: "Fiscal Year"
            //    }
            //});
            //GHGChart.setTitle({text: 'Annual GHG'});
            //GHGChart.redraw();

            $('#annualUseGraph').hide();
            $('#annualCostGraph').hide();
            //$('#annualGHGGraph').hide();
            $('#monthlyAreaGraph').hide();

        }
        Highcharts.setOptions({
            global: {
                useUTC: false
            },
            lang: {
                thousandsSep: ','
            },
            colors: ['#E8E762', '#A2DF0E', '#FDAF02', '#0CB7F7', '#D301FE']
        });




        function makeHighchart(htmlId, pointData){
            //loadSpinner(htmlId);
            $(htmlId).highcharts({
                chart: {
                    type: 'areaspline',
                    events: {
                        load: function(){
                            var chart = this;
                            var extremes = chart.xAxis[0].getExtremes();
                            var dayButton = document.getElementById('dayButton');
                            var weekButton = document.getElementById('weekButton');
                            var monthButton = document.getElementById('monthButton');

                            dayButton.addEventListener('click', function(){
                                chart.xAxis[0].setExtremes(extremes.dataMax - 24 * 3600 * 1000, extremes.dataMax);
                                $('#dayButton').addClass('active');
                                $('#weekButton').removeClass('active');
                                $('#monthButton').removeClass('active');
                            });

                            weekButton.addEventListener('click', function(){
                                chart.xAxis[0].setExtremes(extremes.dataMax - 24 * 3600 * 1000 * 7, extremes.dataMax);
                                $('#dayButton').removeClass('active');
                                $('#weekButton').addClass('active');
                                $('#monthButton').removeClass('active');
                            });

                            monthButton.addEventListener('click', function(){
                                chart.xAxis[0].setExtremes(extremes.dataMin, extremes.dataMax);
                                $('#dayButton').removeClass('active');
                                $('#weekButton').removeClass('active');
                                $('#monthButton').addClass('active');
                            });
                        }
                    }
                },
                lang :{
                    thousandsSep: ','
                },

                title: {
                    text: 'Real Time Graph'
                },
                xAxis: {
                    type: 'datetime',
                    tickmarkPlacement: 'on',
                    dateTimeLabelFormats: {
                        hour: '%l:%M %P',
                        day: '%b %e'
                    },
                    title: {
                        enabled: false,
                        useHtml: true,
                        style:{
                            fontFamily: 'Source Sans Pro'
                        }
                    },
                    min: new Date().getTime() - (24*3600*1000*7),
                },
                yAxis: {
                    opposite: false,
                    title: {
                        text: 'MMBtu'
                    }
                },
                tooltip: {
                    shared: true,
                    xDateFormat: '%b %e, %l:%M%p',
                    valueDecimals: 2,
                    valueSuffix: ' MMBtu'
                },
                legend: {
                    enabled: true
                },
                plotOptions: {
                    area: {
                        stacking: 'normal',
                        lineColor: '#FFFFFF',
                        lineWidth: 1,
                        marker: {
                            lineWidth: 1,
                            lineColor: '#FFFFFF'
                        }
                    },
                    series: {
                        stacking: 'normal',
                        marker: {
                            enabled: false
                        }
                    }
                },
                rangeSelector: {
                    enabled: false,
                    inputEnabled: false
                },
                navigator: {
                    enabled: false
                },
                scrollbar: {
                    enabled: false
                },
                credits:{
                    enabled: false
                },
                series: [{
                    name: 'PG&E Substation',
                    data: pointData[0]
                }, {
                    name: 'Solar Farm',
                    data: pointData[1]
                }, {
                    name: 'Biodigester',
                    data: pointData[2]
                }, {
                    name: 'CHCP Gas',
                    data: pointData[3]
                }, {
                    name: 'PG&E Master Meters',
                    data: pointData[4]
                }]
            });
        }

        function makeStaticChart(htmlId, series, prefix, suffix){
            //loadSpinner(htmlId);
            $(htmlId).highcharts({
                colors: ['#FDAF02', '#A2DF0E', '#E9DF4B', '#0DB1E4', '#D301FE'],
                chart: {
                    type: 'column'
                },
                title: {
                    text: null

                },
                xAxis: {
                    tickmarkPlacement: 'on',
                    categories: ['93-94','94-95','95-96','96-97','97-98','98-99','99-00','00-01','01-02','02-03','03-04','04-05','05-06','06-07','07-08','08-09','09-10','10-11','11-12','12-13','13-14','14-15'],
                },
                tooltip: {
                    //shared: true,
                    valueSuffix: suffix,
                    valuePrefix: prefix
                },

                plotOptions: {
                    area: {
                        stacking: 'normal',
                        lineColor: '#FFFFFF',
                        lineWidth: 1,
                        marker: {
                            lineWidth: 1,
                            lineColor: '#FFFFFF'
                        }
                    },
                    series: {
                        stacking: 'normal',
                        marker: {
                            enabled: false
                        }
                    }
                },

                credits:{
                    enabled: false
                },
                rangeSelector: {
                    enabled: false,
                    inputEnabled: false
                },
                legend: {
                    enabled: true
                },
                yAxis: {
                    opposite: false,
                    title: {
                        text: 'MMBtu',
                        style: {
                            'color': '#444952',
                            //fontWeight: 'bold'
                        }
                    },
                    tickWidth: 0,
                    showFirstLabel: true,
                    endOnTick: true

                },
                navigator: {
                    enabled: false
                },
                scrollbar: {
                    enabled: false
                },
                series: series
            });
        }
    }
]);