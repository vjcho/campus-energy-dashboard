'use strict';
//var ch;
function createDataDependents(category,subcategory){
    var dataObj = {
        userStart: '',
        userEnd: '',
        userId:'',
        userWaterCategory: '',
        userTimeCategory: '',
        userReturnType: '',
        userTimeStep: '',
        userRetrievalType:'',
        userReturnNumVal:''
    };
    if (subcategory === 'DemandNow') {
        dataObj = {
            userStart:'yester',
            userEnd:'piNow',
            userId:'campus',
            userWaterCategory: category,
            userTimeStep:'fiveMinHour',
            userReturnType: 'InterpolatedData',
            userTimeCategory: subcategory,
            userRetrievalType:'line',
            userReturnNumVal:'line'
        };
    } else if (subcategory === 'DemandWeekAvg') {
        dataObj = {
            userStart:'piNow',
            userEnd:'piNow',
            userId:'campus',
            userWaterCategory: category,
            userTimeStep:'none',
            userReturnType: 'Value',
            userTimeCategory: subcategory,
            userRetrievalType:'curr',
            userReturnNumVal:'now'
        };
    } else if (subcategory === 'DemandDayAvg') {
        dataObj = {
            userStart:'piNow',
            userEnd:'piNow',
            userId:'campus',
            userWaterCategory: category,
            userTimeStep:'none',
            userReturnType: 'Value',
            userTimeCategory: subcategory,
            userRetrievalType:'curr',
            userReturnNumVal:'now'
        };
    }         
	return dataObj;
}

function dataDown(){
	$('#demandGraph').empty();
	$('#demandGraph').append('<p style="margin:20px;font-weight:bold;text-align:center;"">*Real-time data will be available soon!</p>');
	$('.spinner').remove();
}

function demandGraph(category) {
    var ch;
	var labelUnit = ' gallons';
	var dataElement = ['curr','week','day','goal'];

	var minValue = 0;
	var dataObj = {};
	var tempDayArray = [], tempWeekArray = [], tempGoalArray = [];
    var monthGoals = [
        ["January", 804, 99],
        ["February", 825, 118],
        ["March", 876, 280],
        ["April", 1048, 413],
        ["May", 1148, 746],
        ["June", 1183, 853],
        ["July", 1273, 971],
        ["August", 1150, 898],
        ["September", 1106, 781],
        ["October", 1142, 438],
        ["November", 855, 203],
        ["December", 807, 116]
    ];
    var today = new Date();
    var legendName = ['Current Demand','Past Week Average','Past 24 Hour Average','Goal for ' + monthGoals[today.getMonth()][0]];

    $.ajax({
        url:'/piRestData',
        data: createDataDependents(category,'DemandNow')
    }).fail(function(jxhr,textStatus,errorThrown){
        console.log('ERROR: data in line graph',errorThrown);
    }).done(function(currPiData) {
        console.log('testing: ' + currPiData);
        $.ajax({
            url:'/piRestData',
            data: createDataDependents(category,'DemandWeekAvg')
        }).fail(function (jxhr,textStatus,errorThrown) {
            console.log('ERROR: data in line graph',errorThrown);
        }).done(function (weekPiData) {
            $.ajax({
                url:'/piRestData',
                data: createDataDependents(category,'DemandDayAvg')
            }).fail(function (jxhr,textStatus,errorThrown) {
                console.log('ERROR: data in line graph',errorThrown);
            }).done(function (dayPiData) {
                dataObj.curr=currPiData.DemandNow;
                dataObj.week=weekPiData.DemandWeekAvg;
                dataObj.day=dayPiData.DemandDayAvg;
                if (category === 'domestic')
                    dataObj.goal = monthGoals[today.getMonth()][1];
                else if (category === 'utility')
                    dataObj.goal = monthGoals[today.getMonth()][2];
                for (var i=0; i<dataObj.curr.length; i++) {
                    tempWeekArray.push([dataObj.curr[i][0],dataObj.week]);
                    tempDayArray.push([dataObj.curr[i][0],dataObj.day]);
                    tempGoalArray.push([dataObj.curr[i][0],dataObj.goal]);
                }
                dataObj.day = tempDayArray;
                dataObj.week = tempWeekArray;
                dataObj.goal = tempGoalArray;

                Highcharts.setOptions({global:{useUTC:false}});
                $('#demandGraph').highcharts({
                    colors: ['rgba(86,202,214,.5)','#3f83c3','#48D1CC','#999', '#aaeeee'],
                    chart: {
                        backgroundColor: null
                    },
                    title: null,
                    xAxis: {
                        type: 'datetime',
                        dateTimeLabelFormats: { minute:'%l:%M%P', hour:'%l:%M%P', day:'%b %e' },
                        gridLineColor: '#707073',
                        gridLineWidth: 0,
                        labels: {
                            style: {
                                color: '#2a2a2b'
                            }
                        },
                        lineColor: '#707073',
                        lineWidth: 2,
                        minorGridLineColor: '#505053',
                        minorGridLineWidth: 0,
                        tickColor: '#707073',
                        tickWidth: 2,
                        title: {
                            style: {
                                color: '#2a2a2b'
                            }
                        }
                    },
                    yAxis: {
                        title: {
                            text: labelUnit,
                            style: {
                               color: '#2a2a2b'
                            }
                        },
                        min: 0,
                        gridLineColor: '#707073',
                        gridLineWidth: 0,
                        labels: {
                            style: {
                               color: '#2a2a2b'
                            }
                        },
                        lineColor: '#707073',
                        lineWidth: 2,
                        minorGridLineColor: '#505053',
                        minorGridLineWidth: 0,
                        tickColor: '#707073',
                        tickWidth: 2
                    },
                    exporting: {
                        enabled: false
                    },
                    tooltip: {
                        backgroundColor: 'rgba(0, 0, 0, 0.85)',
                        style: {
                            color: '#F0F0F0'
                        },
                        crosshairs: true,
                        shared: true,
                        xDateFormat: '%A, %b %e, %l:%M%p',
                        valueSuffix: labelUnit
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                enabled: false,
                                symbol: 'circle',
                                radius: 1
                            },
                            fillOpacity:.9
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    credits: {
                       enabled: false
                   },
                    series: [{
                        name: legendName[0],
                        data: dataObj[dataElement[0]],
                        type: 'area',
                        color: 'rgba(43,158,170,1)'
                    }, {
                        name: legendName[1],
                        data: dataObj[dataElement[1]],
                        type: 'line',
                        color: '#ADE5EA'
                    }, {
                        name: legendName[2],
                        data: dataObj[dataElement[2]],
                        type: 'line',
                        color: '#0F5B6C'
                    }, {
                        name: legendName[3],
                        data: dataObj[dataElement[3]],
                        type: 'line',
                        color: '#8CC63F'
                    }]
                });
            });
        });
	});

}
