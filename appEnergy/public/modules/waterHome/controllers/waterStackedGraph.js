'use strict';

function dataDown(){

    $('#usageGraph').empty();
    $('#usageGraph').append('<p style="margin:20px;font-weight:bold;text-align:center;"">*Real-time data will be available soon!</p>');
    $('.spinner').remove();

}

function graph() {
    // 2015 data
    var currentWaterDataObj = {
        // [january, february, ... , december]
        /*
        domestic: [67,77,72,63,70,45],//,41,44,40,48,53,55],//0,0,0,0,0,0,0],
        utility: [40,51,43,40,21,1],//2,2,2,7,15,23],//0,0,0,0,0,0,0],
        fishery: [26,26,27,25,28,28],//,32,32,26,28,29,13],//0,0,0,0,0,0,0],
        agriculture: [217,202,117,72,12,13]//,0,80,70,99,166,273],//0,0,0,0,0,0,0]
        */

        //calendarized data
        domestic: [65,66,49,40,43,40,50,53,55,60,57,55],
        utility: [42,20,2,2,2,2,7,15,23,24,31,31],
        fishery: [26,26,30,31,32,26,30,26,13,15,9,3],
        agriculture: [60,13,9,1,2,4,27,73,145,133,126,84]
    };
    // 2013 data (calendaraized rounded values)
    var lastYearWaterDataObj = {
        /*
        domestic: [54,86,73,59,68,51,48,47,44,52,60,73],
        utility: [39,65,57,42,26,12,7,6,6,17,24,47],
        fishery: [30,31,32,29,34,32,35,39,32,35,33,34],
        agriculture: [189,146,100,61,12,8,8,0,3,6,41,137]
        */

        domestic: [64,68,49,48,48,44,52,60,68,68,76,68],
        utility: [45,26,12,7,6,6,17,24,44,49,58,53],
        fishery: [31,34,31,35,41,32,35,33,32,35,27,30],
        agriculture: [52,12,8,10,0,3,14,54,137,193,141,91]
    };
    var createGoal = function(waterDataObj){
        var temp = [];
        for (var i=0; i<12; i++) {
            temp.push(Math.round((waterDataObj.utility[i]+
            waterDataObj.domestic[i]+
            waterDataObj.agriculture[i]+
            waterDataObj.fishery[i])*0.75));
        }
        return temp;
    };
    currentWaterDataObj.goal = createGoal(lastYearWaterDataObj);
    Highcharts.setOptions({global:{useUTC:false}});

    //Highcharts.setOptions(Highcharts.theme);
    $('#usageGraph').highcharts({
        colors: ['rgba(15, 91, 108, .75)','#0F5B6C','rgba(43, 158, 170, .75)','#2B9EAA','rgba(77, 77, 77, .75)','#4D4D4D','rgba(86, 202, 214, .75)','#56CAD6','#BDBDBF'],
        chart: {
            type: 'column',
            backgroundColor: null
        },
        exporting: {
            enabled: false
        },
        title: {
            text:null
        },
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: ['Sep 2014', 'Oct', 'Nov', 'Dec 2014','Jan 2015', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug 2015'],
            offset:15,
            labels:{
                useHTML:true,
                style:{
                    paddingTop:'15px',
                    fontSize:'15px',
                    fontWeight:500,
                    color:'#147990',
                    fontFamily:"Source Sans Pro",
                    textAlign: 'center'
                },
                autoRotationLimit: 40,
                autoRotation:false
            },
            gridLineColor: '#707073',
            gridLineWidth: 0,
            lineColor: '#707073',
            lineWidth: 2,
            minorGridLineColor: '#505053',
            minorGridLineWidth: 0,
            tickColor: '#707073',
            tickWidth: 2,
            title: {
                style: {
                    color: '#2a2a2b'
                }
            }
        },
        yAxis: {
            title:{
                text:null
            },
            labels:{
                enabled:false
            },
            lineWidth:0,
            lineColor: null,
            gridLineWidth: 0,
            minorGridLineWidth: 0,
            minorTickLength: 0,
            tickLength: 0,
            stackLabels:{
                enabled:true,
                useHtml:true,
                formatter: function() {
                    if(this.stack == 'Baseline')
                        return '<div style="color:rgba(77,77,77,.75);">2013</div>';
                    else{
                        if(this.x < 4)
                            return '<div style="color:#0F5B6C">2014</div>';
                        else
                            return '<div style="color:#0F5B6C">2015</div>';
                    }

                },
                rotation: -90,
                textAlign: 'center',
                x: 4,
                y: -16,
                style:{
                    fontFamily:"Source Sans Pro",
                    fontSize:'13px',
                    color:'#FFFFFF',
                    weight:900,
                    textShadow:0
                }
            }
        },
        tooltip: {
            useHtml:true,
            backgroundColor: 'rgba(0, 0, 0, 0.85)',
            style: {
                color: '#F0F0F0'
            },
            formatter: function(){
                var year;
                var series = this.series.name.substring(0,this.series.name.indexOf(' '));
                console.log(series);
                if(this.series.options.stack == 'Baseline'){
                    year = '2013';
                    series = this.series.name;
                }
                else if(this.point.x < 4){
                    //console.log(this.point.x);
                    year = '2014';
                    series = this.series.name.substring(0,this.series.name.indexOf(' '));
                }
                else{
                    year = '2015';
                    series = this.series.name.substring(0,this.series.name.indexOf(' '))
                }

                console.log(year);

                return this.key.substring(0,3) + ' ' + year + '<br><b>' + series + ': </b>' + this.point.y + '<p> Mgals</p>' + '<br><b>Total: ' + '</b>' + this.point.total + '<p> Mgals</p>';
            }
        },
        plotOptions: {
            series: {
                marker: {
                    fillColor: '#FFFFFF',
                    lineWidth: 2,
                    radius: 1,
                    enabled: false,
                    lineColor: null
                },
                pointPadding: 0.07,
                groupPadding: 0.12,
                borderWidth: 0,
                shadow: false
            },
            column: {
                stacking: 'normal'
            }

        },
        series: [{
            name: 'Utility',
            data:  lastYearWaterDataObj.utility,
            stack: 'Baseline'
        }, {
            name: 'Utility 2015',
            linkedTo:':previous',
            data: currentWaterDataObj.utility,
            stack: '2015'
        }, {
            name: 'Domestic',
            data: lastYearWaterDataObj.domestic,
            stack: 'Baseline'
        }, {
            name: 'Domestic 2015',
            linkedTo:':previous',
            data: currentWaterDataObj.domestic,
            stack: '2015'
        }, {
            name: 'Agriculture',
            data: lastYearWaterDataObj.agriculture,
            stack: 'Baseline'
        }, {
            name: 'Agriculture 2015',
            linkedTo:':previous',
            data: currentWaterDataObj.agriculture,
            stack: '2015'
        }, {
            name: 'Fishery',
            data: lastYearWaterDataObj.fishery,
            stack: 'Baseline'
        }, {
            name: 'Fishery 2015',
            linkedTo:':previous',
            data: currentWaterDataObj.fishery,
            stack: '2015'
        }, {
            name: 'Goal',
            type: 'line',
            data: currentWaterDataObj.goal
        }]
    });
}