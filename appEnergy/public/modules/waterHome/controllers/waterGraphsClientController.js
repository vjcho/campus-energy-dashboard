'use strict';
angular.module('waterHome').controller('GraphController', ['$scope','$location', '$state',
    function($scope,$location,$state) {

        $('#graphs-button').click(function(){
            $state.reload();
         });

        var flag = 0;

        function loadSpinner(htmlID) {
            var opts = {
                lines: 10, // The number of lines to draw
                length: 7, // The length of each line
                width: 4, // The line thickness
                radius: 10, // The radius of the inner circle
                rotate: 0, // The rotation offset
                color: '#000', // #rgb or #rrggbb
                speed: 1, // Rounds per second
                trail: 60, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9 // The z-index (defaults to 2000000000)
            };
            var spinner = new Spinner(opts).spin();
            var element = document.getElementById(htmlID);
            element.innerHTML = '';
            element.appendChild(spinner.el);
        }
        //demandGraph();
        function createGraphs(name, htmlID, file, category) {
            graph();
            $('.graphTitle').hide();
            $('#demandGraph').hide();
            $('#' + name + 'Button').click(function () {
                $.ajax(file)
                    .fail(function (jxhr, textStatus, errorThrown) {
                        console.log(errorThrown);
                    })
                    .done(function (data, textStatus, jxhr) {
                        var subcategory = 'domestic';
                        if (name === 'demand') {
                            $('.graphTitle').show();
                            $('#choiceBox').val('domestic');
                            $('#usageTable').hide();
                            $('#usageGraph').hide();
                            $('#demandGraph').show();
                            $('#usage-key').hide();
                            $('#demand-key').show();
                            $('#usageButton').removeClass('active');
                            $('#demandButton').addClass('active');
                            //$('#demandGraph').append(spinner.el);
                            loadSpinner('demandGraph');
                            demandGraph(subcategory);
                            $('select').change(function () {
                                console.log('changing the select thing');
                                subcategory = $(this).val();
                                //$('#demandGraph').append(spinner.el);
                                loadSpinner('demandGraph');
                                demandGraph(subcategory);
                            });

                            var demandchart = $('#demandGraph').highcharts();
                            $('#curr-color').click(function(){
                                console.log('flag: ' + flag);
                                console.log('current clicked');
                                demandchart = $('#demandGraph').highcharts();
                                if(demandchart.series[0].visible){
                                    demandchart.series[0].hide();
                                    $('#curr-color').css('background-color','#E0E0E0');
                                    $('#curr-color').css('background-image', 'none');
                                }
                                else {
                                    demandchart.series[0].show();
                                    $('#curr-color').css('background-color','none');
                                    $('#curr-color').css('background-image', 'url(/modules/waterHome/img/redesign/WaterDemand.svg)');
                                }
                            });
                            $('#pastWeek-color').click(function(){
                                demandchart = $('#demandGraph').highcharts();
                                if(demandchart.series[1].visible){
                                    demandchart.series[1].hide();
                                    $('#pastWeek-color').css('background-color','#E0E0E0');
                                }
                                else{
                                    demandchart.series[1].show();
                                    $('#pastWeek-color').css('background-color','#ADE5EA');
                                }
                            });
                            $('#day-color').click(function(){
                                demandchart = $('#demandGraph').highcharts();
                                if(demandchart.series[2].visible){
                                    demandchart.series[2].hide();
                                    $('#day-color').css('background-color','#E0E0E0');
                                }
                                else{
                                    demandchart.series[2].show();
                                    $('#day-color').css('background-color','#0F5B6C');
                                }
                            });
                            $('#goal-color').click(function(){
                                demandchart = $('#demandGraph').highcharts();
                                if(demandchart.series[3].visible){
                                    demandchart.series[3].hide();
                                    $('#goal-color').css('background-color','#E0E0E0');
                                }
                                else{
                                    demandchart.series[3].show();
                                    $('#goal-color').css('background-color','#8CC63F');
                                }
                            });

                            //dataDown();
                        } else if (name === 'usage') {
                            $('.graphTitle').hide();
                            $('#demandGraph').hide();
                            $('#usageTable').show();
                            $('#usageGraph').show();
                            $('#demand-key').hide();
                            $('#usage-key').show();
                            $('#demandButton').removeClass('active');
                            $('#usageButton').addClass('active');
                            //$(htmlID).append(spinner.el);
                            loadSpinner('usageGraph');
                            // $('#'+name+'Graph').show(0);
                            graph();
                            //$('#usageGraph').highcharts().redraw();
                            usagechart = $('#usageGraph').highcharts();

                            //$('#usageGraph').append('<p style="margin:10px;font-weight:bold;font-size:8pt;text-align:center;">*Utility, Agriculture, and Fishery water are estimated values at this time.</p>');
                        }
                    });
            });
        }
        $('#demand-key').hide();
        createGraphs('usage', '#usageGraph', 'modules/waterHome/controllers/waterStackedGraph.js', '');
        createGraphs('demand', '#demandGraph', 'modules/waterHome/controllers/waterLineGraph.js', '');

        //clicky thing to hide/unhide categories
        var usagechart = $('#usageGraph').highcharts();
        $('#domestic-color').click(function(){
            console.log("button clicked");
            if(usagechart.series[2].visible){
                usagechart.series[2].hide();
                //usagechart.series[2].setVisible(false,true);
                usagechart.series[3].hide();
                $('#domestic-color').css('background-color','#E0E0E0');
            }
            else {
                usagechart.series[2].show();
                usagechart.series[3].show();
                $('#domestic-color').css('background-color','#2B9EAA');
            }
        });
        $('#ag-color').click(function(){
            if(usagechart.series[4].visible){
                usagechart.series[4].hide();
                usagechart.series[5].hide();
                $('#ag-color').css('background-color','#E0E0E0');
            }
            else{
                usagechart.series[4].show();
                usagechart.series[5].show();
                $('#ag-color').css('background-color','#4D4D4D');
            }
        });
        $('#util-color').click(function(){
            if(usagechart.series[0].visible){
                usagechart.series[0].hide();
                usagechart.series[1].hide();
                $('#util-color').css('background-color','#E0E0E0');
            }
            else{
                usagechart.series[0].show();
                usagechart.series[1].show();
                $('#util-color').css('background-color','#0F5B6C');
            }
        });
        $('#fish-color').click(function(){
            if(usagechart.series[6].visible){
                usagechart.series[6].hide();
                usagechart.series[7].hide();
                $('#fish-color').css('background-color','#E0E0E0');
            }
            else{
                usagechart.series[6].show();
                usagechart.series[7].show();
                $('#fish-color').css('background-color','#56CAD6');
            }
        });
        $('#goal-line').click(function(){
            if(usagechart.series[8].visible){
                usagechart.series[8].hide();
                $('#goal-line').css('background-color','#E0E0E0');
            }
            else{
                usagechart.series[8].show();
                $('#goal-line').css('background-color','#BDBDBF');
            }
        });

    }
]);
