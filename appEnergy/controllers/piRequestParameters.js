'use strict';
var PiRequestParameters = function(buildingId,graphId,graphType,subCategory) {
	function setTimeInterval(graphType) {
		var timeObj = {startTime:'',endTime:'',timestep:''};
		if (graphType === 'PieMonthly') {
			timeObj.startTime = 'usageMonthlyPieStart';
			timeObj.endTime = 'usageMonthlyPieEnd';
			timeObj.timestep = 'month';
		} else if (graphType === 'PieAnnual') {
			timeObj.startTime = 'usageAnnualPieStart';
			timeObj.endTime = 'usageAnnualPieEnd';
			timeObj.timestep = 'year';
		} else if (graphType === 'Values') {
			timeObj.startTime = 'piNow';
			timeObj.endTime = 'piNow';
			timeObj.timestep = 'now';
		} else if (graphType === 'Line') {
            timeObj.startTime = 'week';
            timeObj.endTime = 'now';
            timeObj.timestep = 'minute';
        } else if (graphType === 'CustomLine') {
            timeObj.startTime = 'tempStart';
            timeObj.endTime = 'tempEnd';
            timeObj.timestep = 'minute';
		} else if (graphType === 'BarMonthly') {
			timeObj.startTime = 'usageMonthlyStart';
			timeObj.endTime = 'usageMonthlyEnd';
			timeObj.timestep = 'month';
		} else if (graphType === 'BarAnnual') {
			timeObj.startTime = 'usageAnnualStart';
			timeObj.endTime = 'usageAnnualEnd';
			timeObj.timestep = 'year';
		}
		return timeObj;
	}
	function setPath(buildingId,graphId,subCategory,graphType) {
		var pathObj = {buildingName:'',subCategory:'',graphCategory:''};
		pathObj.buildingName = buildingId;
		pathObj.subCategory = subCategory;
		pathObj.graphCategory = graphId+graphType;
		return pathObj;
	}
	function setRetrievalProperties(graphType){
		var propertyObj = {retType:'',retNumVal:''};
		if (graphType === 'PieMonthly' || graphType === 'PieAnnual') {
			propertyObj.retType = 'line';
			propertyObj.retNumVal= 'now';
		} else if(graphType === 'Values') {
			propertyObj.retType = 'curr';
			propertyObj.retNumVal = 'now';
		} else if (graphType === 'Line') {
			propertyObj.retType = 'line';
			propertyObj.retNumVal = 'line';
		} else if (graphType === 'BarMonthly') {
			propertyObj.retType = 'line';
			propertyObj.retNumVal= 'bar';
		} else if (graphType === 'BarAnnual') {
			propertyObj.retType = 'line';
			propertyObj.retNumVal = 'bar';
		} else if (graphType === 'CustomLine') {
            propertyObj.retType = 'line';
            propertyObj.retNumVal = 'line';
        }
		return propertyObj;
	}
	this.resId = subCategory;
	this.timeInterval = setTimeInterval(graphType);
	this.pathObj= setPath(buildingId,graphId,subCategory,graphType);
	this.retrievalPropObj = setRetrievalProperties(graphType);
};
module.exports = PiRequestParameters;