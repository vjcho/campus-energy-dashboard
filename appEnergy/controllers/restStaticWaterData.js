'use strict';
module.exports = {
	piChoices: {
		piWebServices:{
			archiveData:'http://piwebservices.ucdavis.edu:88/PIWebServices/PITimeSeries.svc?wsdl',
			searchTags:'http://piwebservices.ucdavis.edu:88/PIWebServices/PISearch.svc?singleWsdl'
		},
        piRestAPI:{
            WebId: {
                protocol:'https',
                hostname:'ucd-pi-iis.ou.ad3.ucdavis.edu',
                pathname:'/piwebapi/points',
                query:{'path':''}
            },
            Value:{
                protocol:'https',
                hostname:'ucd-pi-iis.ou.ad3.ucdavis.edu',
                pathname:'/piwebapi/streams/{webId}/value'
            },
            InterpolatedData:{
                protocol:'https',
                hostname:'ucd-pi-iis.ou.ad3.ucdavis.edu',
                pathname:'/piwebapi/streams/{webId}/interpolated',
                query: {'startTime':'','endTime':'','interval':''}
            }
        }
	},
    campus: {
        staticData: {
            usageBarMonthly: {},
            usageBarAnnual: {}
        },
        piQueryOptions:{
            startTime: {beginWeek:'sun',week:'',yester:'*-24h',now:'*',piNow:'*',usageMonthlyStart:'1-dec-14-1h',usageAnnualStart:'1-jan-13-1s'},
            endTime: {now:'*',piNow:'*',usageMonthlyEnd:'1-apr-15-1h',usageAnnualEnd:'1-jan-15-1s'},
            retrievalType: {line:'Interpolated',curr:'Compressed' },
            numValue: {bar:'25',line:'105000',pie:'100',now:'1'},
            timestep: {now:'',minute:'1m',fiveMinHour:'5M',quarterHour:'15M',hour:'60M',day:'1D',week:'W',month:'1Mo',year:'1y'},
            path: {
                domestic: {
                    DemandNow:'pi:\\\\util-pi-p.ucdavis.edu\\DWtr_Demand_Flow',
                    DemandHourAvg:'pi:\\\\util-pi-p.ucdavis.edu\\DWtr_Demand_Flow1h.avg',
                    DemandDayAvg:'pi:\\\\util-pi-p.ucdavis.edu\\DWtr_Demand_Flow1d.avg',
                    DemandWeekAvg:'pi:\\\\util-pi-p.ucdavis.edu\\DWtr_Demand_Flow1w.avg'
                },
                utility: {
                    DemandNow:'pi:\\\\util-pi-p.ucdavis.edu\\UWtr_Demand_Flow',
                    DemandHourAvg:'pi:\\\\util-pi-p.ucdavis.edu\\UWtr_Demand_Flow1h.avg',
                    DemandDayAvg:'pi:\\\\util-pi-p.ucdavis.edu\\UWtr_Demand_Flow1d.avg',
                    DemandWeekAvg:'pi:\\\\util-pi-p.ucdavis.edu\\UWtr_Demand_Flow1w.avg'
                },
                agriculture: {
                    DemandNow:'pi:\\\\util-pi-p.ucdavis.edu\\AWtr_Demand_Flow',
                    DemandHourAvg:'pi:\\\\util-pi-p.ucdavis.edu\\AWtr_Demand_Flow1h.avg',
                    DemandDayAvg:'pi:\\\\util-pi-p.ucdavis.edu\\AWtr_Demand_Flow1d.avg',
                    DemandWeekAvg:'pi:\\\\util-pi-p.ucdavis.edu\\AWtr_Demand_Flow1w.avg'
                },
                fishery: {
                    DemandNow:'pi:\\\\util-pi-p.ucdavis.edu\\FWtr_Demand_Flow',
                    DemandHourAvg:'pi:\\\\util-pi-p.ucdavis.edu\\FWtr_Demand_Flow1h.avg',
                    DemandDayAvg:'pi:\\\\util-pi-p.ucdavis.edu\\FWtr_Demand_Flow1d.avg',
                    DemandWeekAvg:'pi:\\\\util-pi-p.ucdavis.edu\\FWtr_Demand_Flow1w.avg'
                }
            }
        }
    },
/*
    biodigester: {
        piQueryOptions: {
            startTime: {yester:'*-24h', now:'*'}
        }
    }
*/
};