'use strict';
var jsonData = require('../controllers/restStaticWaterData.js'),
    url = require('url'),
    request = require('request'),
    asyncRequests = require('async');
function createPiTagPath (buildingId,graphCategoryId,dataCategoryId) {
    var piTag = '';
    if (buildingId!==undefined && graphCategoryId!==undefined && dataCategoryId!==undefined && jsonData[buildingId].piQueryOptions.path!==undefined)
        piTag=jsonData[buildingId].piQueryOptions.path[graphCategoryId][dataCategoryId];
    return piTag;
}
function createRestUrl (parameterObj,webId,urlType) {
    var piJsonRef =
        { 'WebId': {
            'protocol':'https',
            'hostname':'ucd-pi-iis.ou.ad3.ucdavis.edu',
            'pathname':'/piwebapi/points',
            'query': {'path':''}
        },
        'Value':{
            'protocol':'https',
            'hostname':'ucd-pi-iis.ou.ad3.ucdavis.edu',
            'pathname':'/piwebapi/streams/{webId}/value'
        },
        'InterpolatedData':{
            'protocol':'https',
            'hostname':'ucd-pi-iis.ou.ad3.ucdavis.edu',
            'pathname':'/piwebapi/streams/{webId}/interpolated',
            'query': {'startTime':'','endTime':'','interval':''}
        }},
        pathParamObj = {};
    if (urlType && jsonData.piChoices.piRestAPI[urlType]!==undefined) pathParamObj = piJsonRef[urlType];
    if (urlType==='WebId' && webId===null) {
        pathParamObj.query.path = createPiTagPath('campus', parameterObj.userWaterCategory, parameterObj.userTimeCategory).substring(3);
    } else if (webId!==null && pathParamObj.pathname.search('{webId}')>-1) {
        pathParamObj.pathname = pathParamObj.pathname.replace('{webId}',webId);
        var queryObj = {};
        if (urlType==='InterpolatedData') {
            var piQueryJsonObj = {};
            piQueryJsonObj = jsonData[parameterObj.userId].piQueryOptions;
            queryObj.startTime = piQueryJsonObj.startTime[parameterObj.userStart];
            queryObj.endTime = piQueryJsonObj.endTime[parameterObj.userEnd];
            queryObj.interval = piQueryJsonObj.timestep[parameterObj.userTimeStep];
        }
        pathParamObj.query = queryObj;
    }
    return url.format(pathParamObj);
}
var requestPI =  {
    restMultiCategoryRequest: function(parameterObj, callback) {
        var tempWebId = '';
        request.get(createRestUrl(parameterObj,null,'WebId'), function (error, response, body) {
            var results = {};
            if (!error && JSON.parse(body).WebId) {
                tempWebId = JSON.parse(body).WebId;
                var dataArray = [],
                    piDateReturnKey = 'Value';
                if (parameterObj.userReturnType.search('Value')>-1) {
                    piDateReturnKey = 'Value';
                    request(createRestUrl(parameterObj,tempWebId,piDateReturnKey),function(error,response,body) {
                        if (JSON.parse(body).Value!==undefined) {
                            //dataArray.push(Math.round(parseFloat(JSON.parse(body).Value)));
                            results[parameterObj.userTimeCategory] = Math.round(parseFloat(JSON.parse(body).Value));
                            callback(null,results);
                        } else {
                            //results[parameterObj.userId][parameterObj.userWaterCategory] = -1;
                            results[parameterObj.userTimeCategory] = -1;
                            console.log('\x1B[35m','ERROR: Value. Incorrect parameter requested. Explicitly choose Value or Stream or Invalid point in PI.','campus',parameterObj.userWaterCategory);
                            callback(null,results); //bad value
                        }
                    });
                } else {
                    piDateReturnKey = 'InterpolatedData';
                    request(createRestUrl(parameterObj,tempWebId,piDateReturnKey),function(error,response,body) {
                        if (JSON.parse(body).Items!==undefined) {
                            var resItems = JSON.parse(body).Items;
                            for (var k=0;k<resItems.length;k++) {
                                if (parameterObj.userTimeCategory.search('Demand')>-1) dataArray.push([Date.parse(resItems[k].Timestamp),Math.round(parseFloat(resItems[k].Value))]);
                                else dataArray.push(Math.round(parseFloat(resItems[k].Value)));
                            }
                            //results[parameterObj.userId][parameterObj.userWaterCategory] = dataArray;
                            results[parameterObj.userTimeCategory] = dataArray;
                            callback(null,results);
                        } else {
                            console.log('\x1B[35m','ERROR: Interpolated. Incorrect parameter requested. Explicitly choose Value or Stream or Invalid point in PI.','campus',parameterObj.userWaterCategory);
                            //results[parameterObj.userId][parameterObj.userWaterCategory] = [];
                            results[parameterObj.userTimeCategory] = [];
                            callback(null,results); //bad request Value/Interpolated or bad value
                        }
                    });
                }
            } else {
                results[parameterObj.userTimeCategory]=[];
                console.log('\x1B[35m',JSON.parse(body).Errors+' campus '+parameterObj.pathObj);
                callback(null,results); //bad webid
            }
        });
    }
};
function formatParameterTypes(paramItem) {
    var paramTypeArray=[];
    if (typeof paramItem==='string') { paramTypeArray.push(paramItem); }
    else if (typeof paramItem==='object') { paramTypeArray=paramItem; }
    return paramTypeArray;
}
exports.getDemandValues  = function(req,res) {
    var parameterArray = [req.query];
    asyncRequests.map(parameterArray,requestPI.restMultiCategoryRequest.bind(requestPI),function(error,results) {
        if (results) {
            var returnValue = results[0];
            res.send(returnValue);
        } else {
            res.send([]);
        }
    });
};