'use strict';
var soap = require('../../other_modules/soap/soap.js'),
	xml2js = require('xml2js'),
	asyncRequests = require('async'),
	jsonData = require('../controllers/restStaticWaterData.js');
function calculateTime (jsonObj,clientStartTime,clientEndTime){
	var piQueryTime = {startTime:'*',endTime:'*'};
	if (clientEndTime==='now') { // must calculate exact time to the second! :(
		var monthArray = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'], // needs 1st-3-letters/month
			nowDate = new Date();
		piQueryTime.endTime = nowDate.getDate().toString()+'-'+monthArray[nowDate.getMonth()]+'-'+nowDate.getFullYear().toString().substring(2,4)+' '+nowDate.toTimeString().substring(0,5)+':00';
		if (clientStartTime==='now')
			piQueryTime.startTime = piQueryTime.endTime;
		else if (clientStartTime==='yester')
			piQueryTime.startTime = piQueryTime.endTime+'-24h';
		else if (clientStartTime==='week')
			piQueryTime.startTime = piQueryTime.endTime+'-7d';
		else
			piQueryTime.startTime = piQueryTime.endTime;
	} else {
		piQueryTime.startTime = jsonObj.startTime[clientStartTime];
		piQueryTime.endTime = jsonObj.endTime[clientEndTime];
	}
	return piQueryTime;
}
function createPiTagPath (buildingId,graphCategoryId,dataCategoryId) {
	var piTag = '';
	if (buildingId!==undefined && graphCategoryId!==undefined && dataCategoryId!==undefined && jsonData[buildingId].piQueryOptions.path!==undefined)
		piTag=jsonData[buildingId].piQueryOptions.path[graphCategoryId][dataCategoryId];
	return piTag;
}
function createXMLRequestMessage (startTime,endTime,path,retrievalType,numValue,timestep) {
	return '<pid:GetPIArchiveData>' +
		'<pid:requests>' +
		'<pid:PIArcDataRequest>' +
		'<pid:TimeRange>' +
		'<pid:Start>' + startTime + '</pid:Start>' +
		'<pid:End>' + endTime + '</pid:End>' +
		'</pid:TimeRange>' +
		'<pid:Path>' + path + '</pid:Path>' +
		'<pid:PIArcManner Updates="false" RetrievalType="' + retrievalType + '" NumValues="' + numValue + '" Boundaries="Inside">' +
		'<pid:TimeStep>' + timestep + '</pid:TimeStep>' +
		'</pid:PIArcManner>' +
		'</pid:PIArcDataRequest>' +
		'</pid:requests>' +
		'</pid:GetPIArchiveData>';
}
function isData (resultsJSON) {
	var dataFlag = 0;
	if (resultsJSON) {
		if (resultsJSON['s:Envelope']['s:Body'][0].GetPIArchiveDataResponse!==undefined) {
			if (resultsJSON['s:Envelope']['s:Body'][0].GetPIArchiveDataResponse[0].GetPIArchiveDataResult[0].TimeSeries[0].TimedValues[0]!=='') {
				dataFlag = 1;
			} //else console.log('No data!');
		} else console.log('Pi query and/or pi tag is incorrect.');
	} else console.log('Pi connection may be down. Results did not return correctly.');
	return dataFlag;
}

function accessSoapData (building,reqMessage,resDataId,resProjId,resDataObj,client,callback) {
	if (client && client.GetPIArchiveData) {
		client.GetPIArchiveData(reqMessage, function (soapErr, soapRes, soapRaw){
			xml2js.parseString(soapRaw, function (soapErr, soapRes){
				if (!soapErr) {
					var dataArray=[];
					if (isData(soapRes)===1){
						var dataPoints = soapRes['s:Envelope']['s:Body'][0].GetPIArchiveDataResponse[0].GetPIArchiveDataResult[0].TimeSeries[0].TimedValues[0].TimedValue;
						if (dataPoints.length>=1){
							if (resProjId.search('DemandNow')>-1){
								var tempDemandArray = [];
								for (var k=0; k<dataPoints.length; ++k) {
									tempDemandArray = dataPoints[k].$.Time.split('.');
									tempDemandArray[0]= tempDemandArray[0].replace('Z','');
									dataArray.push([Date.parse(tempDemandArray[0]), Math.round(parseFloat(dataPoints[k]._))]);
								}
							} else {
								for (var m=0; m<dataPoints.length; ++m) {
									dataArray.push(Math.round(parseFloat(dataPoints[m]._)));
								}
							}
						} else { console.log('\x1b[7m','Something weird happened in data parsing'); }
					}
					resDataObj[resDataId] = dataArray;
				}
				callback(null,resDataObj);
			});
		});
	} else {
		console.log('Pi is down!');
		resDataObj[resDataId] = -1;
		callback(null,resDataObj);
	}
}

var requestPI =  {
	soapMultiCategoryRequest: function(parameterObj, callback) {
		var soapUrl = jsonData.piChoices.piWebServices.archiveData,
			piQueryJsonObj = jsonData[parameterObj.userId].piQueryOptions,
			xmlRequestMessage = createXMLRequestMessage(
				piQueryJsonObj.startTime[parameterObj.userStart],
				piQueryJsonObj.endTime[parameterObj.userEnd],
				createPiTagPath(parameterObj.userId, parameterObj.userWaterCategory, parameterObj.userTimeCategory),
				piQueryJsonObj.retrievalType[parameterObj.userRetrievalType],
				piQueryJsonObj.numValue[parameterObj.userReturnNumVal],
				piQueryJsonObj.timestep[parameterObj.userTimeStep]),
			resultsDataObj = {};
		soap.createClient(soapUrl, function (err, client){
			accessSoapData(parameterObj.userId,xmlRequestMessage,parameterObj.userWaterCategory,parameterObj.userTimeCategory,resultsDataObj,client,function(err,results) {
				var resultsObj = {};
				if (parameterObj.userReturnType.search('Value')>-1) {
					resultsObj[parameterObj.userTimeCategory] = {};
					if (!err) {
						resultsObj[parameterObj.userTimeCategory] = results[parameterObj.userWaterCategory][0];
						callback(null, resultsObj);
					} else {
						resultsObj[parameterObj.userTimeCategory] = -1;
						callback(null, resultsObj); //bad value
					}
				} else {
					if (!err) {
						resultsObj[parameterObj.userTimeCategory] = results[parameterObj.userWaterCategory];
						callback(null, resultsObj);
					} else {
						resultsObj[parameterObj.userTimeCategory] = [];
						callback(null, resultsObj); //bad value
					}
				}
			});
		});
	}
};
function formatParameterTypes(paramItem) {
	var paramTypeArray=[];
	if (typeof paramItem==='string') { paramTypeArray.push(paramItem); }
	else if (typeof paramItem==='object') { paramTypeArray=paramItem; }
	return paramTypeArray;
}
exports.getDemandValues  = function(req,res) {
	var parameterArray = [req.query];
	asyncRequests.map(parameterArray,requestPI.soapMultiCategoryRequest.bind(requestPI),function(error,results) {
		if (results) {
			var returnValue = results[0];
			res.send(returnValue);
		} else {
			res.send([]);
		}
	});
};