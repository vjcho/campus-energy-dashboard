'use strict';
var jsonData = require('../controllers/restStaticWaterData.js'),
    requestParams = require('../controllers/piRequestParameters.js'),
    url = require('url'),
    request = require('request'),
    asyncRequests = require('async');
function calculateTime (jsonObj,clientStartTime,clientEndTime){
    var piQueryTime = {startTime:'*',endTime:'*'};
    if (clientEndTime==='now') { // must calculate exact time to the second! :(
        var monthArray = ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'], // needs 1st-3-letters/month
            nowDate = new Date();
        nowDate.setMinutes(nowDate.getMinutes() - 2);   //subtract 1 minute from current time
        piQueryTime.endTime = nowDate.getDate().toString()+'-'+monthArray[nowDate.getMonth()]+'-'+nowDate.getFullYear().toString().substring(2,4)+' '+nowDate.toTimeString().substring(0,5)+':00';
        if (clientStartTime==='yester')
            piQueryTime.startTime = piQueryTime.endTime+'-24h';
        else if (clientStartTime==='week')
            piQueryTime.startTime = piQueryTime.endTime+'-7d';
        else if (clientStartTime==='month')
            piQueryTime.startTime = piQueryTime.endTime+'-1Mo';
        else
            piQueryTime.startTime = piQueryTime.endTime;
    } else {
        piQueryTime.startTime = jsonObj.startTime[clientStartTime];
        piQueryTime.endTime = jsonObj.endTime[clientEndTime];
    }
    return piQueryTime;
}
function createPiTagPath (buildingId,graphCategoryId,dataCategoryId) {
    var piTag = '';
    if (buildingId!==undefined && graphCategoryId!==undefined && dataCategoryId!==undefined && jsonData[buildingId].piQueryOptions.path!==undefined)
        piTag=jsonData[buildingId].piQueryOptions.path[graphCategoryId][dataCategoryId];
    return piTag;
}
function checkPath(parameterObj){
    var isPath = true;
    if (jsonData[parameterObj.pathObj.buildingName].piQueryOptions.path[parameterObj.pathObj.graphCategory][parameterObj.pathObj.subCategory]===null) isPath=false;
    return isPath;
}
function createRestUrl (parameterObj,webId,urlType) {
    var piJsonRef =
        { 'WebId': {
            'protocol':'https',
            'hostname':'ucd-pi-iis.ou.ad3.ucdavis.edu',
            'pathname':'/piwebapi/points',
            'query': {'path':''}
        },
            'Value':{
                'protocol':'https',
                'hostname':'ucd-pi-iis.ou.ad3.ucdavis.edu',
                'pathname':'/piwebapi/streams/{webId}/value'
            },
            'InterpolatedData':{
                'protocol':'https',
                'hostname':'ucd-pi-iis.ou.ad3.ucdavis.edu',
                'pathname':'/piwebapi/streams/{webId}/interpolated',
                'query': {'startTime':'','endTime':'','interval':''}
            }},
        pathParamObj = {};
    if (urlType && jsonData.campus.piRestAPI[urlType]!==undefined) pathParamObj = piJsonRef[urlType];
    if (urlType==='WebId' && webId===null) {
        pathParamObj.query.path = createPiTagPath(parameterObj.pathObj.buildingName, parameterObj.pathObj.graphCategory, parameterObj.pathObj.subCategory).substring(3);
    } else if (webId!==null && pathParamObj.pathname.search('{webId}')>-1) {
        pathParamObj.pathname = pathParamObj.pathname.replace('{webId}',webId);
        var queryObj = {};
        if (urlType==='InterpolatedData') {
            var timeObj = {}, piQueryJsonObj = {};
            piQueryJsonObj = jsonData[parameterObj.pathObj.buildingName].piQueryOptions;
            timeObj = calculateTime(piQueryJsonObj,parameterObj.timeInterval.startTime,parameterObj.timeInterval.endTime);
            queryObj.startTime = timeObj.startTime;
            queryObj.endTime = timeObj.endTime;
            queryObj.interval = piQueryJsonObj.timestep[parameterObj.timeInterval.timestep];
        }
        pathParamObj.query = queryObj;
    }
    return url.format(pathParamObj);
}
var requestPI =  {
    restMultiCategoryRequest: function(parameterObj, callback) {
        var tempWebId = '';
        var resultsObj = {};
        resultsObj[parameterObj.pathObj.buildingName] = {};
        if (checkPath(parameterObj)){
            request.get(createRestUrl(parameterObj,null,'WebId'), function (error, response, body) {
                if (!error && JSON.parse(body).WebId) {
                    tempWebId = JSON.parse(body).WebId;
                    var dataArray = [],
                        piDateReturnKey = 'Value';
                    if (parameterObj.pathObj.graphCategory.search('Value')>-1) {
                        piDateReturnKey = 'Value';
                        request(createRestUrl(parameterObj,tempWebId,piDateReturnKey),function(error,response,body) {
                            if (JSON.parse(body).Value!==undefined) {
                                dataArray.push(Math.round(parseFloat(JSON.parse(body).Value)));
                                resultsObj[parameterObj.pathObj.buildingName][parameterObj.pathObj.subCategory] = dataArray;
                                callback(null,resultsObj);
                            } else {
                                console.log('ERROR: Incorrect parameter requested. Explicitly choose Value or Stream.');
                                resultsObj[parameterObj.pathObj.buildingName][parameterObj.pathObj.subCategory] = [-1];
                                callback(null,resultsObj); //bad request Value/Interpolated or bad value
                            }
                        });
                    } else {
                        piDateReturnKey = 'InterpolatedData';
                        request(createRestUrl(parameterObj,tempWebId,piDateReturnKey),function(error,response,body) {
                            if (JSON.parse(body).Items!==undefined) {
                                var resItems = JSON.parse(body).Items;
                                for (var k=0;k<resItems.length;k++) {
                                    if (parameterObj.pathObj.graphCategory.search('demand')>-1) dataArray.push([Date.parse(resItems[k].Timestamp),Math.round(parseFloat(resItems[k].Value))]);
                                    else dataArray.push(Math.round(parseFloat(resItems[k].Value)));
                                }
                                resultsObj[parameterObj.pathObj.buildingName][parameterObj.pathObj.subCategory] = dataArray;
                                callback(null,resultsObj);
                            } else {
                                console.log('ERROR: Incorrect parameter requested. Explicitly choose Value or Stream.');
                                resultsObj[parameterObj.pathObj.buildingName][parameterObj.pathObj.subCategory] = [];
                                callback(null,resultsObj); //bad request Value/Interpolated or bad value
                            }
                        });
                    }
                } else {
                    resultsObj[parameterObj.pathObj.buildingName][parameterObj.pathObj.subCategory] = [-1];
                    console.log('\x1B[35m',JSON.parse(body).Errors+' '+parameterObj.pathObj.buildingName +' '+parameterObj.pathObj.subCategory);
                    callback(null,resultsObj); //bad webid
                }
            });
        } else {
            resultsObj[parameterObj.pathObj.buildingName][parameterObj.pathObj.subCategory] = [];
            callback(null,resultsObj);
        }
    }
};
function formatParameterTypes(paramItem) {
    var paramTypeArray=[];
    if (typeof paramItem==='string') { paramTypeArray.push(paramItem); }
    else if (typeof paramItem==='object') { paramTypeArray=paramItem; }
    return paramTypeArray;
}
exports.getBuilding = function(req,res) {
    if (req.query.buildingId && req.query.graphId && req.query.graphType && req.query.dataCategory){
        var parameterArray=[],
            buildingObj={},
            buildingArray = formatParameterTypes(req.query.buildingId),
            categoryArray = formatParameterTypes(req.query.dataCategory);
        if (parameterArray && categoryArray && buildingObj) {
            for (var buildingIndex in buildingArray) {
                buildingObj[buildingArray[buildingIndex]] = {};
                if (jsonData[buildingArray[buildingIndex]]) {
                    for (var categoryIndex in categoryArray) {
                        buildingObj[buildingArray[buildingIndex]][categoryArray[categoryIndex]] = null;
                        parameterArray.push(new requestParams(buildingArray[buildingIndex],req.query.graphId,req.query.graphType,categoryArray[categoryIndex]));

                    }
                }
            }
            asyncRequests.map(parameterArray,requestPI.restMultiCategoryRequest.bind(requestPI),function(err,results) {
                if (results && buildingObj) {
                    for (var arrayItem in results) {
                        for (var buildingItem in results[arrayItem]) {
                            for (var dataItem in results[arrayItem][buildingItem]) {
                                buildingObj[buildingItem][dataItem] = results[arrayItem][buildingItem][dataItem];
                            }
                        }
                    }
                }
                res.send(buildingObj);
            });
        }
    } else { res.send({'error':'teamId and/or dataCategory undefined'}); }
};
