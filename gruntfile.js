'use strict';

module.exports = function(grunt) {
	// Unified Watch Object
    var watchFiles = {
        serverViews: ['app*/views/**/*.html'],
        serverJS: ['gruntfile.js',
            'server.js','webApps.js','constants.js','hosts.js',
            'config/**/*.js',
            'app*/controllers/*.js',
            'app*/models/*.js',
            'app*/routes/*.js'],
        clientViews: ['app*/public/modules/**/views/*.html'],
        clientJS: ['!appEnergy/public/modules/**/*.js','!app*/public/dist/*.js','!app*/public/lib/**/*.js'],
        clientCSS: ['app*/public/modules/**/css/*.css'],
        mochaTests: ['app*/tests/**/*.js'],
        clientSass: ['app*/public/modules/**/*.scss']
    };

	// Project Configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			serverViews: {
				files: watchFiles.serverViews,
				options: {
					livereload: true
				}
			},
			serverJS: {
				files: watchFiles.serverJS,
				tasks: ['jshint'],
				options: {
					livereload: true
				}
			},
			clientViews: {
				files: watchFiles.clientViews,
                tasks: ['uncss','cssmin'],
				options: {
					livereload: true
				}
			},
			clientJS: {
				files: watchFiles.clientJS,
				tasks: ['jshint'],
				options: {
					livereload: true
				}
			},
			clientCSS: {
				files: watchFiles.clientCSS,
                tasks: ['cssmin'],
				options: {
					livereload: true
				}
			},
			minifying: {
				files: watchFiles.clientJS,
				tasks: ['loadConfig','ngmin','uglify'],
				options: {
					livereload: true
				}
			},
			sass: {
				files: watchFiles.clientSass,
				tasks: ['compass:waterDashboard']
			}
		},
		bowercopy: {
            options: {
                srcPrefix: ''
            },
			waterDashboard: {
				files: [
					{ src:'public/lib/bootstrap', dest:'appEnergy/public/lib/bootstrap' },
					{ src:'public/lib/angular', dest:'appEnergy/public/lib/angular' },
					{ src:'public/lib/angular-resource', dest:'appEnergy/public/lib/angular-resource' },
					{ src:'public/lib/angular-animate', dest:'appEnergy/public/lib/angular-animate' },
					{ src:'public/lib/angular-mocks', dest:'appEnergy/public/lib/angular-mocks' },
					{ src:'public/lib/angular-bootstrap', dest:'appEnergy/public/lib/angular-bootstrap' },
					{ src:'public/lib/angular-ui-utils', dest:'appEnergy/public/lib/angular-ui-utils' },
					{ src:'public/lib/angular-ui-router', dest:'appEnergy/public/lib/angular-ui-router' },
					{ src:'public/lib/jquery', dest:'appEnergy/public/lib/jquery' },
                    { src:'public/lib/highstock-release', dest:'appEnergy/public/lib/highstock-release' },
                    { src:'other_modules/spin.min.js', dest:'appEnergy/public/lib/spin.min.js' }
				]
			}
		},
		jshint: {
			all: {
				src: watchFiles.clientJS.concat(watchFiles.serverJS),
				options: {
					jshintrc: true
				}
			}
		},
		//csslint: {
		//	options: {
		//		csslintrc: '.csslintrc'
		//	},
		//	all: {
		//		src: watchFiles.clientCSS
		//	}
		//},
		uglify: {
			production: {
				options: {
					mangle: true
				},
				files: {
					'appEnergy/public/dist/application.min.js': 'appEnergy/public/dist/application.js'
				}
			}
		},
		cssmin: {
			combine: {
				files: {
					'appEnergy/public/dist/application.min.css': '<%= waterCSSFiles %>'
				}
			},
            tidyBootstrap: {
                files: {}
            }
		},
		nodemon: {
			dev: {
				script: 'server.js',
				options: {
					nodeArgs: ['--debug'],
					ext: 'js,html',
					ignore: ['node_modules/**','public/lib/**','app*/public/lib/**'],
					watch: watchFiles.serverViews.concat(watchFiles.serverJS)
				}
			}
		},
		'node-inspector': {
			custom: {
				options: {
					'web-port': 1337,
					'web-host': 'localhost',
					'debug-port': 5858,
					'save-live-edit': true,
					'no-preload': true,
					'stack-trace-limit': 50,
					'hidden': []
				}
			}
		},
        ngmin: {
            production: {
                files: {
					'appEnergy/public/dist/application.js': '<%= waterJavaScriptFiles %>'
                }
            }
        },
		concurrent: {
			default: ['nodemon', 'watch'],
			debug: ['nodemon', 'watch', 'node-inspector'],
			options: {
				logConcurrentOutput: true
			}
		},
		env: {
			test: {
				NODE_ENV: 'test'
			}
		},
		mochaTest: {
			src: watchFiles.mochaTests,
			options: {
				reporter: 'spec',
				require: 'server.js'
			}
		},
		karma: {
			unit: {
				configFile: 'karma.conf.js'
			}
		},
		compass: {
			waterDashboard:{}
		},
        uncss: {}
	});

	// Load NPM tasks 
	require('load-grunt-tasks')(grunt);

	// Making grunt default to force in order not to break the project.
	grunt.option('force', true);

	// A Task for loading the configuration object
	grunt.task.registerTask('loadConfig', 'Task that loads the config into a grunt option.', function() {
		var init = require('./config/init')();
		var waterConfig = require('./config/config')('appEnergy');
		grunt.config.set('waterJavaScriptFiles', waterConfig.assets.js);
		grunt.config.set('waterCSSFiles', waterConfig.assets.css);
	});

	// Default task(s).
	grunt.registerTask('default',['lint','loadConfig','ngmin','concurrent:default']);

	// Debug task.
	grunt.registerTask('debug',['lint','concurrent:debug']);

	// Lint task(s).
	grunt.registerTask('lint',['jshint','csslint']);

	// Build task(s).
	grunt.registerTask('build',['lint','loadConfig','uncss','ngmin','cssmin','uglify','compass']);

	grunt.registerTask('librarycopy',['bowercopy']);

	// Test task.
	grunt.registerTask('test',['env:test','mochaTest','karma:unit']);
};