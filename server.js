'use strict';
var webApps = require('./config/webApps.js'),
    hosts = require('./config/hosts.js');
// initiate a new web app with its domain name and set its port
function startServer(portNum){
    var waterApp = new webApps('appEnergy',['water.ucdavis.edu','wateroncampus.ucdavis.edu']);
    waterApp.setPort(portNum+2);
    var returnArray = [waterApp];
    return returnArray;
}
// make sure to set the environment variable (NODE_ENV)
if (process.env.NODE_ENV){
    var portNum = 1334,
        webAppsArray = startServer(portNum),
        vHostApp = new hosts('vHost',webAppsArray);
    vHostApp.setPort(portNum-1);
}
module.exports.server = startServer;